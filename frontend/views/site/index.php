<?
use yii\widgets\Pjax;
use yii\helpers\Html;
use common\models\User;

/*
 * @var $this yii\web\View
 * @var $dataProvider ActiveDataProvider
 * @var $tab string
 * @var User $client
 */

$this->title = 'My Yii Application';

$view = $tab == 'bill'? '_bill_table' : '_cost_table';
?>
<div class="site-index">
    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h4>Balance: <?= $client->balance->sum ?></h4>
            </div>
        </div>
		<div class="row">
			<div class="col-lg-12">
				<? Pjax::begin(['enablePushState' => false])?>
					<ul id="tabs">
						<li>
							<?= Html::a('Платежи', ['site/index', 'tab' => 'bill'], ['class' => 'tab bill cursor-pointer ' . ($tab == 'bill'? 'active' : 'inactive')]) ?>
						</li>
						<li>
							<?= Html::a('Расходы', ['site/index', 'tab' => 'cost'], ['class' => 'tab cost cursor-pointer ' . ($tab == 'cost'? 'active' : 'inactive')]) ?>
						</li>
					</ul>
					<div class="tab-content">
						<?= $this->render($view, ['dataProvider' => $dataProvider]) ?>
					</div>
				<? Pjax::end() ?>
			</div>
		</div>

    </div>
</div>
