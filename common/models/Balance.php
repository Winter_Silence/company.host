<?php

namespace common\models;

/**
 * This is the model class for table "balance".
 *
 * @property int $id
 * @property int $client_id
 * @property string $sum
 *
 * @property User $client
 */
class Balance extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'balance';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id'], 'integer'],
            [['sum'], 'number'],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['client_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Client ID',
            'sum' => 'Sum',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(User::className(), ['id' => 'client_id']);
    }

    /**
     * @inheritdoc
     * @return BalanceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BalanceQuery(get_called_class());
    }

	/**
	 * @param User $client
	 *
	 * @return bool
	 */
	public static function create(User $client)
	{
		$balance = new self();
		$balance->client_id = $client->id;

		return $balance->save();
    }

	/**
	 * @param float $sum
	 *
	 * @return bool
	 */
	public function changeSumByBillSum($sum)
	{
		$this->sum += $sum;

		return $this->save();
    }
}
