<?
use yii\widgets\ListView;

/**
 * @var $this yii\web\View
 * @var $dataProvider \yii\data\ActiveDataProvider
 */
?>

<?= ListView::widget(['dataProvider' => $dataProvider, 'itemView' => '_bill_list']);
