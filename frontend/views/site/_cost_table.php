<?php
use yii\grid\GridView;

/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 */
?>

<?= GridView::widget(
	[
		'dataProvider' => $dataProvider,
		'columns'      => [
			'id',
			[
				'attribute' => 'date_from',
				'format'    => ['date', 'php:d.m.Y']
			],
			[
				'attribute' => 'date_to',
				'format'    => ['date', 'php:d.m.Y']
			],
			'sum',
		],
		'layout'=>"{items}\n{summary}",
	]
);
?>