<?
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use common\models\User;

/* @var $this yii\web\View
 * @var $dataProvider ActiveDataProvider
 */
?>
<h1>Пользователи</h1>

<p>
	<?= Html::a(Html::button('Создать', ['class' => 'btn btn-primary']), ['create']); ?>
</p>

<?= GridView::widget(
	[
		'dataProvider' => $dataProvider,
		'columns'      => [
			'id',

			'username',
			'email',
			[
				'attribute' => 'status',
				'value' => function(User $model) {
					return $model::getStatuses()[$model->status];
				}
			],
			[
				'attribute' => 'created_at',
				'format'    => ['date', 'php:d.m.Y H:i:s']
			],
			[
				'attribute' => 'updated_at',
				'format'    => ['date', 'php:d.m.Y H:i:s']
			],
			[
				'label' => 'Balance',
				'format' => 'raw',
				'value' => function(User $model) {
					return $model->balance->sum;
				}
			],
			[
				'class'         => 'yii\grid\ActionColumn',
				'headerOptions' => ['width' => '80'],
				'template'      => '{update} {delete} <br/>{bill_list} {cost_list}',
				'buttons' => [
					'bill_list' => function ($url, User $model, $key) {
						return Html::a('Bill', ['bill/list-by-client', 'clientId' => $model->id]);
					},
					'cost_list' => function ($url, User $model, $key) {
						return Html::a('Cost', ['cost/list-by-client', 'clientId' => $model->id]);
					}
				]
			],
		],
	]
) ?>