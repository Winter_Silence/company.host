<?php

namespace common\models;

use yii\db\ActiveRecord;
use yii\behaviors\AttributeBehavior;

/**
 * This is the model class for table "cost".
 *
 * @property int $id
 * @property int $client_id
 * @property string $date_from
 * @property string $date_to
 * @property string $sum
 *
 * @property User $client
 * @property Bill[] $bills
 */
class Cost extends ActiveRecord
{
	const EVENT_CHANGE_COST = 'change_in_cost';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cost';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id'], 'integer'],
            [['date_from', 'date_to'], 'safe'],
            [['sum'], 'number'],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['client_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Client ID',
            'date_from' => 'Date From',
            'date_to' => 'Date To',
            'sum' => 'Sum',
        ];
    }

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			[
				'class' => AttributeBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['date_from'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['date_from'],
				],
				'value' => function ($event) {
					return date('Y-m-d', strtotime($this->date_from));
				},
			],
			[
				'class' => AttributeBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['date_to'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['date_to'],
				],
				'value' => function ($event) {
					return date('Y-m-d', strtotime($this->date_to));
				},
			]
		];
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(User::className(), ['id' => 'client_id']);
    }

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getBills()
	{
		return $this->hasMany(Bill::class, ['client_id' => 'client_id'])->inverseOf('cost');
	}

    /**
     * @inheritdoc
     * @return CostQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CostQuery(get_called_class());
    }
}
