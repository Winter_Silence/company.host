<?php
namespace common\components;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class AdvancedActiveQuery
 *
 * @package common\components
 *
 */
class AdvancedActiveQuery extends ActiveQuery
{
	/**
	 * @var string|bool
	 */
	private $tableAlias = null;

	/**
	 * @var int Счетчик параметров в запросах, нужно для уникальности
	 */
	private static $paramCounter = 0;

	/**
	 * @inheritdoc
	 * @return static|mixed
	 */
	public function alias($alias)
	{
		$this->tableAlias = $alias;

		return parent::alias($alias);
	}

	/**
	 * Возвращает имя таблицы или алиас для использования в запросах
	 *
	 * @return mixed|string
	 */
	protected function getTableAlias()
	{
		if (is_null($this->tableAlias)) {
			$tableName = call_user_func([$this->modelClass, 'tableName']);
			$result = $tableName;

			foreach ((array) $this->from as $alias => $table) {
				if ($table == $tableName && is_string($alias)) {
					$result = $alias;
					break;
				}
			}

			$this->tableAlias = $result;
		}

		return $this->tableAlias;
	}

	/**
	 * Получает уникальное имя парметра для подстановки в запросы
	 *
	 * @return string
	 */
	protected function getUniqParamName()
	{
		return ':alias' . self::$paramCounter++;
	}

	/**
	 * @return bool
	 */
	protected function getPrimaryKeyField()
	{
		/** @var ActiveRecord $modelClass */
		$modelClass = new $this->modelClass();

		$primaryKey = $modelClass->primaryKey();

		if (isset($primaryKey[0])) {
			return $primaryKey[0];
		}

		return false;
	}

	/**
	 * Возвращает имя поля БД, пригодное для использования в запросах
	 *
	 * @param $attribute
	 *
	 * @return string
	 */
	public function field($attribute)
	{
		return implode('.', [$this->getTableAlias(), $attribute]);
	}

	/**
	 * @param            $field
	 * @param            $value
	 * @param bool|false $inverse
	 *
	 * @return $this
	 */
	public function byField($field, $value, bool $inverse = false)
	{
		$fieldName = $this->field($field);
		if ($inverse) {
			if (is_null($value) || is_object($value)) {
				$this->andWhere(['not', [$fieldName => $value]]);
			} else {
				$inverseCondition = is_array($value) ? 'not in' : '!=';
				$this->andWhere([$inverseCondition, $fieldName, $value]);
			}

		} else {
			$this->andWhere([$fieldName => $value]);
		}

		return $this;
	}

	/**
	 * @param $value
	 * @param $inverse
	 *
	 * @return static|ActiveQuery|$this
	 */
	public function byPk($value, bool $inverse = false)
	{
		$primaryKey = $this->getPrimaryKeyField();
		if ($primaryKey !== false) {
			$this->byField($primaryKey, $value, $inverse);
		}

		return $this;
	}
}