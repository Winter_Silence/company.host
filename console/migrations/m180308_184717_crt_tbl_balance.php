<?php

use yii\db\Migration;
use common\models\User;

/**
 * Class m180308_184717_crt_tbl_balance
 */
class m180308_184717_crt_tbl_balance extends Migration
{
	private $_tblName = 'balance';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->createTable($this->_tblName, [
	    	'id' => $this->primaryKey()->unsigned(),
		    'client_id' => $this->integer(),
		    'sum' => $this->decimal(14,2)->defaultValue(0)
	    ]);

	    $this->addForeignKey('balance_client_fk', $this->_tblName, 'client_id', User::tableName(), 'id', 'RESTRICT');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('balance_client_fk', $this->_tblName);
        $this->dropTable($this->_tblName);
    }
}
