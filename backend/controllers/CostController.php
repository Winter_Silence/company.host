<?php

namespace backend\controllers;

use common\components\BillingAction;
use common\models\Cost;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use Yii;

class CostController extends \yii\web\Controller
{
	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['list-by-client', 'create', 'delete', 'update', 'view', 'all-list'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	/**
	 * @param integer $clientId
	 *
	 * @return string
	 */
	public function actionListByClient($clientId)
	{
		$query = Cost::find()->where(['client_id' => $clientId]);
		$dataProvider = new ActiveDataProvider(['query' => $query]);

		return $this->render('index', ['dataProvider' => $dataProvider, 'clientId' => $clientId]);
	}

	/**
	 * @return string
	 */
	public function actionAllList()
	{
		$query = Cost::find()->joinWith(['client']);
		$dataProvider = new ActiveDataProvider(['query' => $query]);

		return $this->render('index', ['dataProvider' => $dataProvider, 'clientId' => false]);
	}

	/**
	 * @param $clientId
	 *
	 * @return string
	 * @throws \Exception
	 * @throws \yii\db\Exception
	 */
	public function actionCreate($clientId)
	{
		$cost = new Cost(['client_id' => $clientId]);
		if ($cost->load(Yii::$app->request->post()) && $cost->save() && BillingAction::changeCost($cost)) {
			$this->redirect(['cost/list-by-client', 'clientId' => $clientId]);
		}

		return $this->render('create', ['model' => $cost]);
	}

	/**
	 * @param $id
	 *
	 * @return string
	 * @throws \Exception
	 * @throws \yii\db\Exception
	 */
	public function actionUpdate($id)
	{
		$cost = $this->findModel($id);

		if ($cost->load(Yii::$app->request->post()) && $cost->save() && BillingAction::changeCost($cost)) {
			$this->redirect(['cost/list-by-client', 'clientId' => $cost->client_id]);
		}

		return $this->render('update', ['model' => $cost]);
	}

	/**
	 * @param $id
	 *
	 * @return bool
	 * @throws \Exception
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDelete($id)
	{
		$cost = $this->findModel($id);
		$clientId = $cost->client_id;
		if ($cost->delete()) {
			$this->redirect(['cost/list-by-client', 'clientId' => $clientId]);
		}

		return false;
	}

	/**
	 * @param $id
	 *
	 * @return array|Cost|null|\yii\db\ActiveRecord
	 */
	private function findModel($id)
	{
		if (!($model = Cost::find()->where(['id' => $id])->one())) {
			throw new NotFoundHttpException('Указанная страница не найдена.');
		}

		return $model;
	}

}
