<?php

namespace backend\controllers;

use common\components\BillingAction;
use common\models\Bill;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use Yii;

class BillController extends \yii\web\Controller
{
	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['create', 'delete', 'update', 'list-by-client', 'view', 'all-list'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	/**
	 * @param integer $clientId
	 *
	 * @return string
	 */
    public function actionListByClient($clientId)
    {
    	$query = Bill::find()->where(['client_id' => $clientId]);
    	$dataProvider = new ActiveDataProvider(['query' => $query]);

        return $this->render('index', ['dataProvider' => $dataProvider, 'clientId' => $clientId]);
    }

	public function actionAllList()
	{
		$query = Bill::find()->joinWith(['client']);
		$dataProvider = new ActiveDataProvider(['query' => $query]);

		return $this->render('index', ['dataProvider' => $dataProvider, 'clientId' => false]);
    }

	/**
	 * @param $clientId
	 *
	 * @return string
	 * @throws \Exception
	 */
	public function actionCreate($clientId)
	{
		$bill = new Bill(['client_id' => $clientId]);
		if ($bill->load(Yii::$app->request->post()) && $bill->save() && BillingAction::changeBill($bill)) {
			$this->redirect(['bill/list-by-client', 'clientId' => $clientId]);
		}

		return $this->render('create', ['model' => $bill]);
    }

	/**
	 * @param $id
	 *
	 * @return string
	 * @throws \Exception
	 */
	public function actionUpdate($id)
	{
		$bill = $this->findModel($id);

		if ($bill->load(Yii::$app->request->post()) && $bill->save() && BillingAction::changeBill($bill)) {
			$this->redirect(['bill/list-by-client', 'clientId' => $bill->client_id]);
		}

		return $this->render('update', ['model' => $bill]);
    }

	/**
	 * @param $id
	 *
	 * @return array|Bill|null|\yii\db\ActiveRecord
	 */
    private function findModel($id)
    {
	    if (!($model = Bill::find()->byPk($id)->one())) {
		    throw new NotFoundHttpException('Указанная страница не найдена.');
	    }

	    return $model;
    }
}
