<?php

use yii\db\Migration;
use common\models\User;

/**
 * Class m180308_181623_crt_tbl_bill
 */
class m180308_181623_crt_tbl_bill extends Migration
{
	private $_tblName = 'bill';

    public function safeUp()
    {
	    $this->createTable($this->_tblName, [
	    	'id' => $this->primaryKey()->unsigned(),
		    'client_id' => $this->integer(),
		    'type_id' => $this->tinyInteger()->unsigned(),
		    'date_from' => $this->date(),
		    'date_to' => $this->date(),
		    'sum' => $this->decimal(14,2)
	    ]);

	    $this->addForeignKey('bill_client_fk', $this->_tblName, 'client_id', User::tableName(), 'id', 'RESTRICT');
	    $this->createIndex('period_bill_indx', $this->_tblName, ['date_from', 'date_to']);
	    $this->createIndex('period_bill_by_client_indx', $this->_tblName, ['date_from', 'date_to', 'client_id']);
    }

    public function safeDown()
    {
    	$this->dropForeignKey('bill_client_fk', $this->_tblName);
        $this->dropTable($this->_tblName);
    }
}
