<?
use common\models\Cost;
use yii\grid\GridView;
use yii\helpers\Html;

/*
 * @var $this yii\web\View
 * @var $dataProvider ActiveDataProvider
 * @var integer $clientId
 */

$columns = ['id'];
if (!$clientId) {
	$columns[] = [
		'attribute' => 'Client',
		'format' => 'raw',
		'value'     => function (Cost $model) {
			return Html::a($model->client->username, ['user/update', 'id' => $model->client_id]);
		}
	];
}
$columns = array_merge(
	$columns,
	[
		[
			'attribute' => 'date_from',
			'format'    => ['date', 'php:d.m.Y']
		],
		[
			'attribute' => 'date_to',
			'format'    => ['date', 'php:d.m.Y']
		],
		'sum',
		[
			'class'         => 'yii\grid\ActionColumn',
			'headerOptions' => ['width' => '80'],
			'template'      => '{update} {delete}',
		]
	]
);
?>
<? if ($clientId): ?>
	<h1>Расходы клиента</h1>
	<p>
		<?= Html::a(Html::button('Создать', ['class' => 'btn btn-primary']), ['cost/create', 'clientId' => $clientId]); ?>
	</p>
<? else: ?>
	<h1>Расходы клиентов</h1>
<? endif; ?>

<?= GridView::widget(
	[
		'dataProvider' => $dataProvider,
		'columns'      => $columns
	]);
?>
