<?
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\User;

/* @var $this yii\web\View
 * @var $form yii\bootstrap\ActiveForm
 * @var $model \frontend\models\LoginForm */

?>

<div class="row">
	<div class="col-lg-5">
		<? $form = ActiveForm::begin(['id' => 'user-form']); ?>

		<?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

		<?= $form->field($model, 'email') ?>

		<?= $form->field($model, 'password')->passwordInput() ?>

		<?= $form->field($model, 'status')->dropDownList(User::getStatuses()); ?>

		<div class="form-group">
			<?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
		</div>

		<? ActiveForm::end(); ?>
	</div>
</div>