<?php
namespace frontend\models;

use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class UserForm extends Model
{
	const SCENARIO_ADMIN_EDIT = 'admin_edit';
	const SCENARIO_SIGNUP = 'signup';

    public $username;
    public $email;
    public $password;
	public $status;
	public $_user;

	public function __construct(User $user = null, $config = [])
	{
		if ($user) {
			$this->_user = $user;
			$this->username = $user->username;
			$this->email = $user->email;
			$this->status = $user->status;
		}

		parent::__construct($config);
	}

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
	    return [
		    ['username', 'trim'],
		    ['username', 'required'],
		    [
			    'username',
			    'unique',
			    'targetClass' => '\common\models\User',
			    'message'     => 'This username has already been taken.',
			    'on' => self::SCENARIO_SIGNUP
		    ],
		    ['username', 'string', 'min' => 2, 'max' => 255],

		    ['email', 'trim'],
		    ['email', 'required'],
		    ['email', 'email'],
		    ['email', 'string', 'max' => 255],
		    [
			    'email',
			    'unique',
			    'targetClass' => '\common\models\User',
			    'message'     => 'This email address has already been taken.',
			    'on' => self::SCENARIO_SIGNUP
		    ],

		    ['password', 'required', 'on' => self::SCENARIO_DEFAULT],
		    ['password', 'string', 'min' => 6],
		    ['password', 'safe', 'on' => self::SCENARIO_ADMIN_EDIT],

		    ['status', 'integer']
	    ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        
        return $user->save() ? $user : null;
    }

	/**
	 * @return User|null
	 */
	public function update()
	{
		if (!$this->validate()) {
			return null;
		}

		$this->_user->username = $this->username;
		$this->_user->email = $this->email;
		$this->_user->status = $this->status;
		if ($this->password) {
			$this->_user->setPassword($this->password);
			$this->_user->generateAuthKey();
		}

		return $this->_user->save() ? $this->_user : null;
    }
}
