<?php

namespace backend\controllers;

use common\models\User;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use frontend\models\UserForm;
use Yii;
use yii\web\NotFoundHttpException;

class UserController extends \yii\web\Controller
{
	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['create', 'delete', 'update', 'index', 'view'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	/**
	 * @return string
	 */
    public function actionCreate()
    {
	    $model = new UserForm(null, ['scenario' => UserForm::SCENARIO_SIGNUP]);
	    if ($model->load(Yii::$app->request->post()) && $model->signup()) {
		    $this->redirect('/admin/user/index');
	    }

        return $this->render('create', ['model' => $model]);
    }

	/**
	 * @param $id
	 *
	 * @return bool
	 * @throws NotFoundHttpException
	 * @throws \Exception
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
    public function actionDelete($id)
    {
    	$model = $this->getUser($id);
    	if ($model->delete()) {
    		$this->redirect(['index']);
	    }

	    return false;
    }

	/**
	 * @return string
	 */
    public function actionIndex()
    {
	    $dataProvider = new ActiveDataProvider(['query' => User::find()->with('balance')]);

	    return $this->render('index', ['dataProvider' => $dataProvider]);
    }

	/**
	 * @param $id
	 *
	 * @return string
	 * @throws NotFoundHttpException
	 */
    public function actionUpdate($id)
    {
	    $user = $this->getUser($id);
	    $model = new UserForm($user, ['scenario' => UserForm::SCENARIO_ADMIN_EDIT]);
	    if ($model->load(Yii::$app->request->post()) && $model->update()) {
		    $this->redirect('/admin/user/index');
	    }

        return $this->render('update', ['model' => $model]);
    }

	/**
	 * @param $id
	 *
	 * @return User
	 * @throws NotFoundHttpException
	 */
	private function getUser($id)
	{
		if (!($user = User::find()->where(['id' => $id])->one())) {
			throw new NotFoundHttpException('Указанная страница не найдена.');
		}

		return $user;
    }
}
