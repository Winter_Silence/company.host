<?php
/**
 * Created by PhpStorm.
 * User: Winter_Silence
 * Date: 10.03.2018
 * Time: 20:16
 */

namespace common\components;

use common\models\Balance;
use yii\base\BootstrapInterface;
use yii\base\Event;
use yii\db\ActiveRecord;

class ClientEvents implements BootstrapInterface
{
	public function bootstrap($app)
	{
		Event::on(User::class, ActiveRecord::EVENT_AFTER_INSERT, [$this, 'onCreateClient']);
	}

	/**
	 * @param $event
	 */
	public function onCreateClient($event)
	{
		Balance::create($event->sender);
	}
}