<?
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\Bill;
use dosamigos\datepicker\DateRangePicker;

/*
 * @var $this yii\web\View
 * @var $form yii\bootstrap\ActiveForm
 * @var $model \common\models\Cost
 */

?>

<div class="row">
	<div class="col-lg-5">
		<? $form = ActiveForm::begin(['id' => 'cost-form']); ?>

		<?= $form->field($model, 'date_from')->widget(DateRangePicker::className(), [
				'attributeTo' => 'date_to',
				'form' => $form,
				'language' => 'ru',
				'size' => 'md',
				'clientOptions' => [
					'autoclose' => true,
					'format' => 'dd-mm-yyyy',
				],
			]
		) ?>

		<?= $form->field($model, 'sum'); ?>

		<div class="form-group">
			<?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'save-button']) ?>
		</div>

		<? ActiveForm::end(); ?>
	</div>
</div>