<?php

namespace console\controllers;

use common\models\Balance;
use common\models\Bill;
use common\models\BillQuery;
use common\models\CostQuery;
use common\models\User;
use yii\console\Controller;
use Yii;
use yii\db\Exception;

class BillsController extends Controller
{
	/**
	 * @param $dateFrom
	 * @param $dateTo
	 *
	 * @throws Exception
	 */
	public function actionCalculate($dateFrom, $dateTo)
	{
		$clients = User::find()
			->alias('u')
			->addSelect(['u.id', 'costsSum' => 'SUM(c.sum)', 'balanceSum' => 'balance.sum'])
			->joinWith(
				[
					'costs' => function (CostQuery $query) use ($dateFrom, $dateTo) {
						$query->alias('c')
							->byPeriod($dateFrom, $dateTo)
							->joinWith(
								[
									'bills' => function (BillQuery $query) {
										$query->onCondition(
											[
												'AND',
												'c.date_from' . ' >= ' . $query->field('date_from'),
												'c.date_to' . ' <= ' . $query->field('date_to')
											]
										)
											->byField('id', null);
									}
								],
								false,
								'LEFT OUTER JOIN'
							);
					}
				],
				false,
				'INNER JOIN'
			)
			->joinWith('balance', false, 'INNER JOIN')
			->groupBy('u.id')
			->asArray()
			->all();

		foreach ($clients as $client) {
			$transaction = Yii::$app->db->beginTransaction();
			try {
				$billSum = $client['costsSum'] - $client['balanceSum'];
				if ($client['balanceSum'] > 0) {
					// Очень плохой код
					$balance = Balance::findOne(['client_id' => $client['id']]);
					if ($balance) {
						$balance->sum = $billSum > 0 ? 0 : -$billSum;
						if (!$balance->save()) {
							throw new Exception(print_r($balance->getErrors(), true));
						}
					}
				}
				if ($billSum > 0) {
					$bill = new Bill();
					$bill->client_id = $client['id'];
					$bill->date_from = $dateFrom;
					$bill->date_to = $dateTo;
					$bill->sum = $billSum;
					$bill->type_id = Bill::TYPE_POSTPAYMENT;
					if (!$bill->save()) {
						throw new Exception(print_r($bill->getErrors(), true));
					}
				}

				$transaction->commit();
			} catch (Exception $e) {
				$transaction->rollBack();
				echo $e->getMessage() . PHP_EOL;
			}
		}
	}
}