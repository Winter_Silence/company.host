<?php

use yii\db\Migration;
use common\models\User;

/**
 * Class m180308_183525_crt_tbl_cost
 */
class m180308_183525_crt_tbl_cost extends Migration
{
	private $_tblName = 'cost';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->createTable($this->_tblName, [
	    	'id' => $this->primaryKey()->unsigned(),
		    'client_id' => $this->integer(),
		    'date_from' => $this->date(),
		    'date_to' => $this->date(),
		    'sum' => $this->decimal(14,2)
	    ]);

	    $this->addForeignKey('cost_client_fk', $this->_tblName, 'client_id', User::tableName(), 'id', 'RESTRICT');
	    $this->createIndex('cost_period_indx', $this->_tblName, ['date_from', 'date_to']);
	    $this->createIndex('cost_period_by_client_indx', $this->_tblName, ['date_from', 'date_to', 'client_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('cost_client_fk', $this->_tblName);
        $this->dropTable($this->_tblName);
    }
}
