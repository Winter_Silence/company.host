<?
use common\models\User;

/*
 * @var $this yii\web\View
 * @var $model User
 */
?>
<h1>Редактирование пользователя</h1>

<?= $this->render('_form', ['model' => $model]); ?>
