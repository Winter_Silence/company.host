<?php

use yii\db\Migration;
use common\models\User;

/**
 * Class m180308_185006_crt_tbl_project
 */
class m180308_185006_crt_tbl_project extends Migration
{
	private $_tblName = 'project';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->createTable($this->_tblName, [
	    	'id' => $this->primaryKey()->unsigned(),
		    'client_id' => $this->integer()
	    ]);

	    $this->addForeignKey('project_client_fk', $this->_tblName, 'client_id', User::tableName(), 'id', 'RESTRICT');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('project_client_fk', $this->_tblName);
        $this->dropTable($this->_tblName);
    }
}
