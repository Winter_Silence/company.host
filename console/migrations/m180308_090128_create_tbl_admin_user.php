<?php

use yii\db\Migration;
use common\models\AdminUser;
/**
 * Class m180308_090128_create_tbl_admin_user
 */
class m180308_090128_create_tbl_admin_user extends Migration
{
	private $_tblName = 'admin_user';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->createTable($this->_tblName,
		    [
			    'id'                   => $this->primaryKey()->unsigned(),
			    'login'                => $this->string(),
			    'auth_key'             => $this->string(32),
			    'password_hash'        => $this->string(),
			    'password_reset_token' => $this->string(),
			    'email'                => $this->string(),
			    'status'               => $this->smallInteger()->defaultValue(AdminUser::STATUS_ACTIVE),
			    'created_at'           => $this->integer(),
			    'updated_at'           => $this->integer()
		    ]
	    );

	    $this->createIndex('admin_login_indx', $this->_tblName, 'login');
	    $this->createIndex('admin_email_indx', $this->_tblName, 'email');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->_tblName);
    }
}
