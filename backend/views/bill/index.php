<?
use common\models\Bill;
use yii\grid\GridView;
use yii\helpers\Html;

/*
 * @var $this yii\web\View
 * @var $dataProvider ActiveDataProvider
 * @var integer $clientId
 */

$columns = ['id'];
if (!$clientId) {
	$columns[] = [
		'attribute' => 'Client',
		'format' => 'raw',
		'value'     => function (Bill $model) {
			return Html::a($model->client->username, ['user/update', 'id' => $model->client_id]);
		}
	];
}
$columns = array_merge(
	$columns,
	[
		[
			'attribute' => 'Type',
			'value'     => function (Bill $model) {
				return $model::getTypes()[$model->type_id];
			}
		],
		[
			'attribute' => 'date_from',
			'format'    => ['date', 'php:d.m.Y']
		],
		[
			'attribute' => 'date_to',
			'format'    => ['date', 'php:d.m.Y']
		],
		'sum',
		[
			'class'         => 'yii\grid\ActionColumn',
			'headerOptions' => ['width' => '80'],
			'template'      => '{update}',
		]
	]
);
?>
<? if ($clientId): ?>
<h1>Платежи клиента</h1>
<p>
	<?= Html::a(Html::button('Создать', ['class' => 'btn btn-primary']), ['bill/create', 'clientId' => $clientId]); ?>
</p>
<? else: ?>
<h1>Платежи клиентов</h1>
<? endif; ?>

<?= GridView::widget(
	[
		'dataProvider' => $dataProvider,
		'columns'      => $columns
	]);
?>
