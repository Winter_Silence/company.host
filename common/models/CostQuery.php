<?php

namespace common\models;

use common\components\AdvancedActiveQuery;

/**
 * This is the ActiveQuery class for [[Cost]].
 *
 * @see Cost
 */
class CostQuery extends AdvancedActiveQuery
{
	/**
	 * @param $clientId
	 *
	 * @return $this
	 */
	public function byClientId($clientId)
	{
		return $this->andWhere(['client_id' => $clientId]);
    }

	/**
	 * @param $dateFrom
	 * @param $dateTo
	 *
	 * @return $this
	 */
	public function byPeriod($dateFrom, $dateTo)
	{
		return $this->andWhere([
			'AND',
			['>=', $this->field('date_from'), $dateFrom],
			['<=', $this->field('date_to'), $dateTo]
		]);
    }

	public function billsByPeriod()
	{
		return $this->onCondition(
			[
				'AND',
				$this->field('date_from') . ' >= ' . Bill::tableName() . '.date_from',
				$this->field('date_to') . ' <= ' . Bill::tableName() . '.date_to',
			]
		);
    }
}
