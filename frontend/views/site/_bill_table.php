<?php
use common\models\Bill;
use yii\grid\GridView;

/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 */
?>

<?= GridView::widget(
	[
		'dataProvider' => $dataProvider,
		'columns'      => [
			'id',
			[
				'attribute' => 'Type',
				'value'     => function (Bill $model) {
					return $model::getTypes()[$model->type_id];
				}
			],
			[
				'attribute' => 'date_from',
				'format'    => ['date', 'php:d.m.Y']
			],
			[
				'attribute' => 'date_to',
				'format'    => ['date', 'php:d.m.Y']
			],
			'sum',
		],
		'layout'=>"{items}\n{summary}",
	]
);
?>