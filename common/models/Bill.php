<?php

namespace common\models;

use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "bill".
 *
 * @property int $id
 * @property int $client_id
 * @property int $type_id
 * @property string $date_from
 * @property string $date_to
 * @property string $sum
 *
 * @property User $client
 * @property Cost $cost
 */
class Bill extends ActiveRecord
{
	const TYPE_PREPAYMENT = 1;
	const TYPE_POSTPAYMENT = 2;

	const EVENT_CHANGE_BILLING = 'change_in_billing';

	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bill';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id'], 'integer'],
            [['sum'], 'number'],
            [['date_from', 'date_to'], 'safe'],
            [['type_id'], 'integer', 'max' => 3],
            [['type_id'], 'isInDatePeriod'],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['client_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Client ID',
            'type_id' => 'Type ID',
            'date_from' => 'Date From',
            'date_to' => 'Date To',
            'sum' => 'Sum',
        ];
    }

	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
			[
				'class' => AttributeBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['date_from'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['date_from'],
				],
				'value' => function ($event) {
					return date('Y-m-d', strtotime($this->date_from));
				},
			],
			[
				'class' => AttributeBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['date_to'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['date_to'],
				],
				'value' => function ($event) {
					return date('Y-m-d', strtotime($this->date_to));
				},
			]
		];
	}

	/**
	 * Проверка уникальности платежа по типу и промежутку времени
	 *
	 * @param $attribute
	 * @param $value
	 *
	 * @return bool
	 */
	public function isInDatePeriod($attribute, $value)
	{
		$result = true;
		$dateFrom = date('Y-m-d', strtotime($this->date_from));
		$dateTo = date('Y-m-d', strtotime($this->date_to));

		if (self::find()->checkPeriod($dateFrom, $dateTo)->byTypeId($this->type_id)->one()) {
			$this->addError('type_id', 'В этом периоде уже есть платёж с таким типом');

			$result = false;
		}

		return $result;
	}

	/**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(User::class, ['id' => 'client_id']);
    }

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCost()
	{
		return $this->hasMany(Cost::class, ['client_id' => 'client_id']);
    }

    /**
     * @inheritdoc
     * @return BillQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BillQuery(get_called_class());
    }

	/**
	 * @return array
	 */
	public static function getTypes(): array
	{
		return [
			self::TYPE_PREPAYMENT => 'предоплата',
			self::TYPE_POSTPAYMENT => 'постоплата',
		];
    }

	/**
	 * @param $billId
	 *
	 * @return int
	 */
    public static function getCostsSumByBillId($billId): int
    {
    	$sum = self::find()->withCost()->byPk($billId)->select('SUM(cost.sum)')->scalar();

    	return $sum ?: 0;
    }
}
